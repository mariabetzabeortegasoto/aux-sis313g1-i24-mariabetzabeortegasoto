import Image from "next/image";
import styles from "./page.module.css";
import Button from "@/components/Button";
import Pokemon from "@/components/Pokemon";
import Video from "@/components/Video";

export default function Home() {
  return (
    <main className={styles.main}>
     <Button/>
     <Pokemon title="Bulbasaur" imageUrl="/images/Pokemon.jpg" type="Planta y Veneno" color="Verde" weight="6,9kg" height="0,7m" ability="Espesura" category="Semilla" />
      <div className="videos">
        <div className="one">
         <Video title="Black Swan" autor="BTS" video="0lapF4DQPKQ" />
        </div>
        <div className="two">
         <Video title="Fake Love" autor="BTS" video="7C2z4GqqS5E"/>
        </div>
        <div className="three">
         <Video title="DNA" autor="BTS" video="MBdVXkSdhwU"/>
        </div>
      </div>
   </main>
  )
}



export default function Video(props:{title: string, autor: string, video:string }){
    return(
        <div>
            <h2>{props.title}</h2>
            <p>Autor: {props.autor}</p>
         <div>
            <iframe
            width="100%"
            height="400"
            src={`https://www.youtube.com/embed/${props.video}`}
            title={props.title}
            frameBorder="0"
            allowFullScreen></iframe>
         </div>
        </div>
    )
}
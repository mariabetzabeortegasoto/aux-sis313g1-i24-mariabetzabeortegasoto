export default function Pokemon(props:{title: string, imageUrl: string, type: string, color: string, weight: string,height:string,ability:string, category:string}){
    return(
        <div className="pokemon">
            <h1>{props.title}</h1>
            <img src={props.imageUrl} alt="" />
            <p>tipo: {props.type}</p>
            <p>color:{props.color}</p>
            <p>peso:{props.weight}</p>
            <p>altura:{props.height}</p>
            <p>habilidad:{props.ability}</p>
            <p>categoria:{props.category}</p>
        </div>
    )
}

# ***PRÁCTICA 4***
## **Universitaria:** Univ.Maria Betzabe Ortega Soto
### **Diseño y programación gráfica: SIS-313** 
### **Fecha: 26/03/2024** 
 - [X] ~~Nivel 10~~
![paractica4](https://i.postimg.cc/ZRTgdvKj/nivel10.png)
 - [X] ~~Nivel 18~~
![paractica4](https://i.postimg.cc/Qd1nYJF0/nivel18.png)
 - [x] ~~Nivel 20~~
![paractica4](https://i.postimg.cc/MGw3CfRx/nivel20.png)
 - [X] ~~Nivel 29~~
![paractica4](https://i.postimg.cc/xdPpnjym/nivel29.png)
 - [X] ~~Nivel 30~~
![paractica4](https://i.postimg.cc/x8ZFhsGF/nivel30.png)
 - [x] ~~Nivel 40~~
![paractica4](https://i.postimg.cc/Fsf634Nt/nivel40.png)
 - [X] ~~Nivel 45~~
![paractica4](https://i.postimg.cc/MKhFYFpn/nivel45.png)
 - [X] ~~Nivel 50~~
![paractica4](https://i.postimg.cc/wv0rcx1z/nivel50.png)
 - [x] ~~Nivel 60~~
![paractica4](https://i.postimg.cc/4yyS8Cc9/nivel60.png)
 - [X] ~~Nivel 66~~
![paractica4](https://i.postimg.cc/dQNXh1Mm/nivel66.png)
 - [X] ~~Nivel 70~~
![paractica4](https://i.postimg.cc/3RSL0LqJ/nivel70.png)
 - [x] ~~Nivel 78~~
![paractica4](https://i.postimg.cc/8c7ZRR8d/nivel78.png)
 - [X] ~~Nivel 80~~
![paractica4](https://i.postimg.cc/J0SPPsmf/nivel80.png)









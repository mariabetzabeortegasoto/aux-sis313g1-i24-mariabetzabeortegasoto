   🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃                                      
#             ***PRÁCTICA 5***φ(*￣0￣)
---
## **Universitaria:** Univ.Maria Betzabe Ortega Soto
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
---
### **Diseño y programación gráfica: SIS-313** 
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
---
### **Fecha: 26/03/2024** 
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 
---

### - [x] ~~Nivel 1~~☆
![practica5](https://i.postimg.cc/pdcqL9MK/1.png)

```
Comando:plate
```
### - [X] ~~Nivel 2~~☆☆
![practica5](https://i.postimg.cc/cHKTgsd2/2.png)
```
Comando:bento
```

### - [X] ~~Nivel 3~~☆☆☆
![practica5](https://i.postimg.cc/jq7ZRdgJ/3.png)
```
Comando:#fancy
```
### - [x] ~~Nivel 4~~☆☆☆☆
![practica5](https://i.postimg.cc/W12nmm2M/4.png)
```
Comando:plate apple
```
### - [X] ~~Nivel 5~~☆☆☆☆☆
![practica5](https://i.postimg.cc/VLP4vQpZ/5.png)
```
Comando:#fancy pickle
```
### - [X] ~~Nivel 22~~(°ー°〃)
![practica5](https://i.postimg.cc/c47h8kNx/22.png)
```
Comando:plate:nth-of-type(2n+3)
```

### - [X] ~~Nivel 23~~(°ー°〃)
![practica5](https://i.postimg.cc/9QJBL4Ms/23.png)
```
Comando:plate apple.small:only-of-type
```

### - [x] ~~Nivel 24~~(。_ 。)
![practica5](https://i.postimg.cc/XJTL9TJJ/24.png)
```
Comando:apple:last-of-type , orange:last-of-type
```

### - [X] ~~Nivel 25~~(＠_＠;)
![practica5](https://i.postimg.cc/0Nj0F8Kj/25.png)
```
Comando:bento:empty
```

### - [x] ~~Nivel 26~~(#_<-)
![practica5](https://i.postimg.cc/D0Lg6BJc/26.png)
```
Comando:apple:not(.small)
```

### - [X] ~~Nivel 27~~ ゜ー゜
![practica5](https://i.postimg.cc/YSPzwJQ3/27.png)
```
Comando:[for]
```
### - [x] ~~Nivel 28~~o_o
![practica5](https://i.postimg.cc/TwWq2C9X/28.png)
```
Comando:plate[for]
```

### - [X] ~~Nivel 29~~（￣︶￣）⁜
![practica5](https://i.postimg.cc/nr44BrgR/29.png)
```
Comando:bento[for="Vitaly"]
```

### - [x] ~~Nivel 30~~O(∩_∩)O
![practica5](https://i.postimg.cc/QCGgF14K/30.png)
```
Comando:[for^="Sa"]
```

### - [X] ~~Nivel 31~~ ( *︾▽︾)
![practica5](https://i.postimg.cc/GhjPpBk9/31.png)
```
Comando:[for$="ato"]
```

### - [X] ~~Nivel 32~~( $ _ $ )
![practica5](https://i.postimg.cc/L4LBrQ5t/32.png)

```
Comando:[for*="obb"]
```
#  Finψ(｀∇´)ψ
![practica5](https://i.postimg.cc/WbV6jmqQ/final.png)
🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃 



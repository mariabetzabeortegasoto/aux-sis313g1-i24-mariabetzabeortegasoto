#  HOMEWORK-8 - SIS313 ψ(｀∇´)ψ
* ### Nombre Completo: _Maria Betzabe Ortega Soto_
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 
* ### CI: _10476574_
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 
* ### RU: _114234_
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 

**1. Capturas ($ _ $)**
![hmw8](https://i.postimg.cc/y6n3vRyp/nodeandnpm.png)
---

**2. Video ♪(´▽｀)**
Video de la practica 8 [Video](https://drive.google.com/file/d/1dpWHZ8TDLqY4pKV50TAeXvRGsJSTj1ig/view?usp=sharing).
---

**3. Comandos (☆▽☆)**
``` 
⁙ mkdir
⁙ cd
⁙ npm --version
⁙ node --version
⁙ npx create-next-app@latest
⁙ npm run dev
```
---

**4. Errores ಠ_ಠ**
```
 ⁙ Practica:
   ✕Permisos(ejecutar como administrador).
   ✕falta de carpeta npm: C:/Users/PC/AppData/Roaming/npm 
⁙ Laboratorio:
   ✕Problemas por internet.

```
![hmw8](https://i.postimg.cc/JhMCtsHZ/error.png)

---
**5. Existen ocasiones, en las en el repositorio (gitlab), una carpeta se torna de color
naranja y no se puede acceder al mismo, tal y como se muestra en la imagen.
Este mismo proyecto no permite ser clonado y en caso de éxito en la clonación, la
carpeta aparece vacía.
Averigüe la causa del problema y la solución del mismo ಥ_ಥ**

```
Causa: Se debe cuando se crea otra carpeta .git dentro de otra. 
solución: Borrar la carpeta y crear otra. 
```







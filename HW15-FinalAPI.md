##  𓇢𓆸 HOMEWORK 15 𓇢𓆸

 ### Nombre Completo: _Maria Betzabe Ortega Soto_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟  
 ### CI: _10476574_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟   
 ### RU: _114234_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟  

---

## 𓃻 Investigar los mensajes de GET, POST y PUT 

### ¿Qué son? 𓂃 ִֶָ𐀔
- **GET**: 𓆸 Recupera datos de un servidor. Es idempotente y segura.
- **POST**: 𓇢 Envía datos para ser procesados a un recurso especificado. No es idempotente.
- **PUT**: 𓆙 Actualiza un recurso actual con nuevos datos. Puede crear un recurso si no existe.

### ¿Para qué sirven? 𓂃 ִֶָ𐀔
- **GET**: 𓆞 Se usa para solicitar datos, como obtener una página web.
- **POST**: 𓆳 Se usa para enviar formularios o subir datos.
- **PUT**: 𓃰 Se usa para actualizar recursos existentes o crear nuevos.

---

## ଳ ¿Para qué sirven los códigos de respuesta HTTP? 

Los códigos de respuesta HTTP son importantes para entender el resultado de una petición HTTP. Estas indican si una solicitud HTTP específica se ha completado con éxito, caso contrario, proporcionan información sobre lo que salió mal.

## •ﻌ• Los códigos de respuesta HTTP se agrupan en 5 clases, investigue cuales son y que números abarcan. 

Los códigos de respuesta HTTP se agrupan en cinco clases:

1. **🎙100-199 (Respuestas informativas )**: 
   - Estos códigos indican que la solicitud fue recibida y el proceso continúa.
   
2. **ᯓ★ 200-299 (Respuestas satisfactorias)**:
   - Estos códigos indican que la solicitud fue recibida, comprendida y aceptada correctamente.

3. **꩜  300-399(Redirecciones )**: 
   - Estos códigos indican que se deben tomar acciones adicionales para completar la solicitud.

4. **✖ 400-499(Errores de los clientes)**: 
   - Estos códigos indican que la solicitud contiene sintaxis incorrecta o no puede ser procesada.

5. **⟳ 500-599 (Errores de los Servidores)**  
   - Estos códigos indican que el servidor falló al cumplir con una solicitud aparentemente válida.

---


## ⌕ Investigar los siguientes códigos de respuesta HTTP, mencione el nombre del código y explique que indica el mismo:

### 200 OK
- **Nombre**: OK
- **Descripción**: 𓇼 La solicitud ha tenido éxito. El significado de un éxito varía dependiendo del método HTTP

### 400 Bad Request
- **Nombre**: Solicitud Incorrecta
- **Descripción**: 𓂀 Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.

### 401 Unauthorized
- **Nombre**: No Autorizado
- **Descripción**: 🗝 El cliente debe autenticarse para obtener la respuesta solicitada.

### 403 Forbidden
- **Nombre**: Prohibido
- **Descripción**: 🚷 El cliente no tiene derechos de acceso al contenido, por lo que el servidor está rechazando otorgar una respuesta apropiada.

### 404 Not Found
- **Nombre**: No Encontrado
- **Descripción**: 🔍 El servidor no puede encontrar el recurso solicitado.

### 408 Request Timeout
- **Nombre**: Tiempo de Solicitud Agotado
- **Descripción**: 🕰 El servidor quiere desconectar esta conexión sin usar.

### 500 Internal Server Error
- **Nombre**: Error Interno del Servidor
- **Descripción**: ⚠️ El servidor ha encontrado una situación que no sabe cómo manejar.

### 501 Not Implemented
- **Nombre**: No Implementado
- **Descripción**: 🏁 El método solicitado no está soportado por el servidor y no puede ser manejado. Los únicos métodos que los servidores requieren soporte son GET y HEAD.

### 502 Bad Gateway
- **Nombre**: Puerta de Enlace Incorrecta
- **Descripción**: 🌐 Esta respuesta de error significa que el servidor, mientras trabaja como una puerta de enlace para obtener una respuesta necesaria para manejar la petición, obtuvo una respuesta inválida.

### 504 Gateway Timeout
- **Nombre**: Tiempo de Espera de la Puerta de Enlace Agotado
- **Descripción**: ⏲️ Esta respuesta de error es dada cuando el servidor está actuando como una puerta de enlace y no puede obtener una respuesta a tiempo.

---

## 🏁 ¿Qué es un Endpoint?

Un **endpoint** es una URL específica (Uniform Resource Locator) que proporciona acceso a un recurso en un servidor. Es donde una API (Interfaz de Programación de Aplicaciones) puede acceder a los recursos que necesita para realizar su función. Los endpoints son esenciales en la comunicación entre clientes y servidores, definiendo dónde deben dirigirse las solicitudes y cómo manejarlas.

---
                         ‧₊˚✧[fIN]✧˚₊‧
---

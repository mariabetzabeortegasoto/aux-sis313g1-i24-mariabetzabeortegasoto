#  HOMEWORK-8 – SIS313 O(∩_∩)O  
* ### Nombre Completo: _Maria Betzabe Ortega Soto_
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 
* ### CI: _10476574_
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 
* ### RU: _114234_
☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆ 
**1. Video o_o**
Video de la practica 7 [Video](https://drive.google.com/file/d/11gd_NZoY_mP_IbwznXI3xJpppU_1UI87/view?usp=sharing).

---

**2. Capturas ￣︶￣**
![hmw8](https://i.postimg.cc/LsbDw7S5/carpeta.png)
![hmw8](https://i.postimg.cc/rs6JFrKL/carpeta2.png)
![hmw8](https://i.postimg.cc/Hx0JXc6m/carpeta3.png)
---

**3. Comandos (☆▽☆)**
``` 
⁙ mkdir
⁙ cd
⁙ dir
⁙ cd ..
```
---

**4. Comandos en Windows y Linux (☆▽☆)**
``` 
⁙ WINDOWS:
1. cd: Sirve para cambiar de directorio, y aumentando dos puntos (..)para retroceder de directorio.
2. dir: El comando lista el contenido del directorio o carpeta donde te encuentras, mostrando todas las subcarpetas o archivos que tiene.
3. mkdir: Crea una carpeta.
4. help: Muestra todos los comandos que hay disponibles, poniendo en cada uno una breve descripción en inglés.
5. Move: Mueve archivos.
⁙ LINUX:
1. ls: Visualiza la lista de los archivos y directorios del sistema.
2. pwd: Imprime la ruta del directorio de trabajo actual.
3. rmdir: Borra un directorio vacío en Linux.
4. cp: Copia archivos o directorios.
5. mv: Mueve o renombra archivos y directorios.

```









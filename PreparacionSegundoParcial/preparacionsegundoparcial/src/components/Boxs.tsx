export default function Boxs(props:{type:string,name:string, value?:string, placeholder:string, for: string, b:string, id?:string}){
    return(
        <div className="Container-Box">
            <label htmlFor = {props.for}><b>{props.b}</b></label>
            <input className="Controles" type={props.type} name={props.name} value={props.value} placeholder={props.placeholder} id={props.id} /> 
        </div>
    )
}
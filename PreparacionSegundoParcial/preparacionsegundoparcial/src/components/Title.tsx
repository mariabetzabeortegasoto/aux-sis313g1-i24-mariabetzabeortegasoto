export default function Title(props:{title: string, paragraph:string }){
    return(
        <div className="Title-principal">
            <h1>{props.title}</h1>
            <p>{props.paragraph}</p>
        </div>
    )

}
'use client';
import Image from "next/image";
import styles from "./page.module.css";
import Boxs from "@/components/Boxs";
import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Title from "@/components/Title";
import Link from "next/link";

const Login = () => {
    return(
      <main>
         <Logo/>
          <section className="Login">
            <div>
                <Title title="Login" paragraph="Login to access your travelwise  account"/>
                  <div className="Container-Box-1">
                    <Boxs for="email" b="email" type="text" name="email" placeholder="email" id="email" />
                    <Boxs for="psw" b="Password" type="Password" name="Password" placeholder="Password" id="psw"/>
                </div>
                <div className=" Direction">
                  <p>Remember me</p>
                  <a href="#">Forgot Password</a>
                </div>
                  <Button loginorsingup="Login"/>
                <div className="ofLogintosingup">
                    <p>Don’t have an account?</p>
                    <a href="/Singup">Sign up</a>
                </div>
                 <div className="top-icons">
                  <Image src="/images/vector.svg" alt=""width={100} height={100} />
                  <h4>Or login with</h4>
                  <Image src="/images/vector.svg" alt="" width={100} height={100} />
                  </div>
                  <div className="icons">
                    <Image src="/images/Facebook.svg" alt="" width={100} height={100} />
                    <Image src="/images/google.svg" alt=""width={100} height={100}/>
                    <Image src="/images/apple.svg" alt="" width={100} height={100}/>
                  </div>
            </div>
            <div>
                <Image src="/images/image-login.svg" alt="" width={500} height={500} />
            </div>
          </section>
      </main> 
    )
}
export default Login
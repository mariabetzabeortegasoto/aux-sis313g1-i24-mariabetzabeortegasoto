'use client';
import Image from "next/image";
import styles from "./page.module.css";
import Logo from "@/components/Logo";
import Boxs from "@/components/Boxs";
import Title from "@/components/Title";
import Button from "@/components/Button";
import { useRef } from 'react';
import Link from 'next/link'

export default function Home() {
  const myRef:any = useRef(null);
  return (
    <main className={styles.main}>
      <Logo/>
        <section className="Login">
          <div>
              <Title title="Login" paragraph="Login to access your travelwise  account"/>
                <div className="Container-Box-1">
                  <Boxs for="email" b="email" type="text" name="email" placeholder="email" id="email" />
                  <Boxs for="psw" b="Password" type="Password" name="Password" placeholder="Password" id="psw"/>
              </div>
              <div className=" Direction">
                <p>Remember me</p><a href="#">Forgot Password</a>
                <Button loginorsingup="Login"/>
                <p>Don’t have an account?</p>
                <a href="/Singup">Sign up</a>
              </div>
              <div className="top-icons">
                <img src="/images/vector.svg" alt="" />
                <h4>Or login with</h4>
                <img src="/images/vector.svg" alt="" />
                <div className="icons">
                  <img src="/images/Facebook.svg" alt="" />
                  <img src="/images/google.svg" alt=""/>
                  <img src="/images/apple.svg" alt="" />
                </div>
              </div>
          </div>
          <div>
              <img src="/images/image-login.svg" alt="" width={100} height={100} />
          </div>
        </section>
    </main>
  );
}

'use client';
import Image from "next/image";
import Boxs from "@/components/Boxs";
import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Title from "@/components/Title";
import Link from "next/link";

const Signup = () => {
    return(
      <main>
        <div className="LogoInSingup">
         <Logo/>
        </div>
          <section className="Sing-Up">
          <div>
                <Image src="/images/image-singup.svg" alt="" width={700} height={700} />
            </div>
            <div>
                <Title title="Sing Up" paragraph="Let’s get you all st up so you can access your personal account."/>
                  <div>
                    <div className="Container-Box-1">
                      <Boxs for="First Name" b="First Name" type="text" name="First Name" placeholder="First Name" id="First Name" />
                      <Boxs for="Last Name" b="Last Name" type="text" name="Last Name" placeholder="Last Name" id="Last Name" />
                    </div>
                    <div className="Container-Box-2">
                      <Boxs for="email" b="email" type="text" name="email" placeholder="email" id="email" />
                      <Boxs for="Phone Number" b="Phone Number" type="text" name="Phone Number" placeholder="Phone Number" id="Phone Number" />
                    </div>
                    <div className="Container-Box-3">
                      <Boxs for="psw" b="Password" type="Password" name="Password" placeholder="Password" id="psw"/>
                      <Boxs for="Confirm Password" b="Confirm Password" type="Password" name="Confirm Password" placeholder="Confirm Password" id="Confirm Password"/>
                    </div>
                </div>
                <div className=" Direction">
                  <p>I agree to all the </p><a href="#">Terms</a> <p>and</p> <a href="#">Privacy Policies</a>
                  </div>
                  <Button loginorsingup="Create account"/>
                  <div className="ofLogintosingup">
                    <p>Already have an account?</p>
                    <a href="/Login">Login </a>
                  </div>
                <div className="top-icons">
                  <Image src="/images/vector.svg" alt="" width={50} height={50} />
                  <h4>Or Sign up with</h4>
                  <Image src="/images/vector.svg" alt=""  width={50} height={50}/>
                </div>
                  <div className="icons">
                    <Image src="/images/Facebook.svg" alt="" width={150} height={150}/>
                    <Image src="/images/google.svg" alt="" width={150} height={150}/>
                    <Image src="/images/apple.svg" alt=""  width={150} height={150}/>
                  </div>
            </div>
          </section>
      </main> 
    )
}
export default Signup
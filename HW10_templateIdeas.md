##  𓇢𓆸 HOMEWORK 10 𓇢𓆸

 ### Nombre Completo: _Maria Betzabe Ortega Soto_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟  
 ### CI: _10476574_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟   
 ### RU: _114234_
𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟𓆝 𓆟 𓆞 𓆝 𓆟  

### Link del template →
 ---
 𓃠 🀦 [PrimerTemplate](https://freebiesbug.com/figma-freebies/real-estate-design/) 𓃠 🀦

---
 ---
𖡼.𖤣𖥧 [SegundoTemplate](https://freebiesbug.com/figma-freebies/plant-ecommerce-template/) 𖡼.𖤣𖥧
 
---
### Link del proyecto en Figma →
 ---
 𓃠 🀦 [PrimerProyectoEnFigma](https://www.figma.com/file/moN5gX927CCzdWiI9MOmrE/SK-Builders---Real-Estate-Website-(Community)?type=design&node-id=1-7890&mode=design&t=QsboowKXzfdne4fe-0) 𓃠 🀦

---
 ---
𖡼.𖤣𖥧 [SegundoProyectoEnFigma](https://www.figma.com/file/Ndql287R8tmPxKzEh57ovM/E-Commerce-Plant-Shop-Website-(Community)?type=design&node-id=0-1&mode=design&t=BeDDWjandUcvGyiH-0) 𖡼.𖤣𖥧
 
---
### Link del nuevo repositorio →
---
𓂀  [NuevoRepositorio](https://gitlab.com/mariabetzabeortegasoto/react-with-mariabetzabeortegasoto.git)  𓂀

---
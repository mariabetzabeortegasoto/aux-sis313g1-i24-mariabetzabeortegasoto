# ***PRÁCTICA 3***
## **Universitaria:** Univ.Maria Betzabe Ortega Soto 
### **Diseño y programación gráfica- SIS-313**
## Nivel Difícil:
- [x] ~~Nivel 21~~
![paractica3](https://i.postimg.cc/CLp1cMdR/nivel21.png)
- [X] ~~Nivel 22~~
![paractica3](https://i.postimg.cc/8PdC98rt/nivel22.png)
- [X] ~~Nivel 23~~
![paractica3](https://i.postimg.cc/rwQwySj4/nivel23.png)
- [x] ~~Nivel 24~~
![paractica3](https://i.postimg.cc/VkYNc7q1/nivel24.png)
## Nivel Medio:
- [x] ~~Nivel 21~~
![paractica3](https://i.postimg.cc/bNQPm3dn/nivel21medio.png)
- [X] ~~Nivel 22~~
![paractica3](https://i.postimg.cc/g00Pkpsy/nivel22medio.png)
- [X] ~~Nivel 23~~
![paractica3](https://i.postimg.cc/k5hP2HY0/nivel23medio.png)
- [x] ~~Nivel 24~~
![paractica3](https://i.postimg.cc/MTCkDQSt/nivel24medio.png)





